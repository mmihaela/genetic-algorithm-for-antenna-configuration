package helper;

import genetic_info.Chromosome;

import java.awt.Point;
import java.awt.geom.Point2D;

public class FitnessUtils {

	/**
	 * Find the most south-estical point from the chromosome
	 * 
	 * @param chromosome
	 * @return the point
	 */
	public static Point2D mostSEpoint(Chromosome chromosome) {
		Point2D ptSE = new Point2D.Double(Utils.N_ROWS, Utils.N_COLS);
		int[] values = chromosome.getValue();
		int line, column;

		for (int i = 0; i < values.length; ++i) {
			line = i / Utils.N_COLS;
			column = i % Utils.N_COLS;
			if (values[i] == 1 && line < ptSE.getX() && column < ptSE.getY()) {
				ptSE.setLocation(line, column);
			}
		}

		return ptSE;
	}

	/**
	 * Find the most south-vestical point from the chromosome
	 * 
	 * @param chromosome
	 * @return the point
	 */
	public static Point2D mostSVpoint(Chromosome chromosome) {
		Point2D ptSV = new Point2D.Double(Utils.N_ROWS, -1);
		int[] values = chromosome.getValue();
		int line, column;

		for (int i = 0; i < values.length; ++i) {
			line = i / Utils.N_COLS;
			column = i % Utils.N_COLS;
			if (values[i] == 1 && line < ptSV.getX() && column > ptSV.getY()) {
				ptSV.setLocation(line, column);
			}
		}

		return ptSV;
	}

	/**
	 * Find the most north-vestical point from the chromosome
	 * 
	 * @param chromosome
	 * 
	 * @return the point
	 */
	public static Point2D mostNVpoint(Chromosome chromosome) {
		Point2D ptNV = new Point2D.Double(-1, -1);
		int[] values = chromosome.getValue();
		int line, column;

		for (int i = 0; i < values.length; ++i) {
			line = i / Utils.N_COLS;
			column = i % Utils.N_COLS;
			if (values[i] == 1 && line > ptNV.getX() && column > ptNV.getY()) {
				ptNV.setLocation(line, column);
			}
		}

		return ptNV;
	}

	/**
	 * Find the most north-estical point from the chromosome
	 * 
	 * @param chromosome
	 * 
	 * @return the point
	 */
	public static Point2D mostNEpoint(Chromosome chromosome) {
		Point2D ptNE = new Point2D.Double(-1, Utils.N_COLS);
		int[] values = chromosome.getValue();
		int line, column;

		for (int i = 0; i < values.length; ++i) {
			line = i / Utils.N_COLS;
			column = i % Utils.N_COLS;
			if (values[i] == 1 && line > ptNE.getX() && column < ptNE.getY()) {
				ptNE.setLocation(line, column);
			}
		}

		return ptNE;
	}

	/**
	 * Compute fitness according to the direction
	 * 
	 * @param chromosome
	 *            the chromosome for which the fitness will be computed
	 * @param ref
	 *            antenna
	 * @param direction
	 *            direction of the signa
	 * @return cost of the antenna
	 */
	public static int fitnessDependingDirection(Chromosome chromosome,
			Point2D ref, int direction) {
		double distance = 0;
		double maxDistance = -1;

		int line, column;
		int noShadowed;
		int[] values = chromosome.getValue();

		boolean validPosition = false;

		for (int i = 0; i < values.length; ++i, validPosition = false) {
			line = i / Utils.N_COLS;
			column = i % Utils.N_COLS;

			//skip inactive cells
			if (values[i] == 0 || (ref.getX() == line && ref.getY() == column)) {
				continue;
			}

			//verify if the cell is in a valid position according to the given direction
			switch (direction) {
			case Utils.NE_DIR:
				if (line <= ref.getX() && column >= ref.getY())
					validPosition = true;
				break;
			case Utils.NV_DIR:
				if (line <= ref.getX() && column <= ref.getY())
					validPosition = true;
				break;
			case Utils.SE_DIR:
				if (line >= ref.getX() && column >= ref.getY())
					validPosition = true;
				break;
			case Utils.SV_DIR:
				if (line >= ref.getX() && column <= ref.getY())
					validPosition = true;
				break;
			}

			if (!validPosition)
				continue;

			// point isn't higher than the antenna
			if (Utils.initialMatrix[line][column] > Utils.initialMatrix[(int) ref
					.getX()][(int) ref.getY()])
				continue;

			distance = Utils.distance(ref, new Point(line, column));

			// the point doesn't exceed the maximum power
			if (distance <= Utils.P_MAX && distance >= Utils.P_MIN) {
				// substract the number of shadowed points from the distance
				noShadowed = Utils.noPoint2DShadowed(ref, new Point2D.Double(
						line, column), direction);
				distance -= noShadowed;
			}

			if (distance > maxDistance)
				maxDistance = distance;
		}

		if (Utils.powerCost.get((int) maxDistance) == null)
			return 0;

		// return the cost corresponding to this distance
		return Utils.powerCost.get((int) maxDistance);
	}

}
