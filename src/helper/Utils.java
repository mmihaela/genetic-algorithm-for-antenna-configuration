package helper;

import java.awt.geom.Point2D;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

public class Utils {
	public static int N_ROWS;
	public static int N_COLS;
	public static int MAX_HEIGHT;
	public static int N_ANTENNAS;
	public static int BUDGET;
	public static int P_MIN;
	public static int P_MAX;
	public static String inputFilename;
	public static String outputFilename;

	// a place is eligible or not
	public static final int ACTIVE = 1;
	public static final int INACTIVE = 0;

	public static HashMap<Integer, Integer> powerCost;
	public static int initialMatrix[][];
	public static int CROSSOVER_POINT = (N_ROWS * N_COLS - 1) / 3;

	// antenna directions
	public static final String SE = "SE";
	public static final String SV = "SV";
	public static final String NE = "NE";
	public static final String NV = "NV";

	// directions
	public static final int SE_DIR = 1;
	public static final int SV_DIR = 2;
	public static final int NE_DIR = 0;
	public static final int NV_DIR = 3;

	// elitism value
	public static final int ELITISM = 0;

	public static final int MAX_NO_ITER = 1500;

	public static final String OUTPUT_FILENAME = "output";

	public static final int EXCEEDED_COST = 10000;

	public static void setToZero(int[] arrayChromosome) {
		for (int i = 0; i < N_ROWS * N_COLS; ++i) {
			arrayChromosome[i] = INACTIVE;
		}
	}

	/**
	 * Compute the euclidian distance between two Point2Ds
	 * 
	 * @param p1
	 *            first Point2D
	 * @param p2
	 *            second Point2D
	 * @return distance between Point2Ds
	 */
	public static double distance(Point2D p1, Point2D p2) {
		double xSquare = (p1.getX() - p2.getX()) * (p1.getX() - p2.getX());
		double ySquare = (p1.getY() - p2.getY()) * (p1.getY() - p2.getY());

		return Math.sqrt(xSquare + ySquare);
	}

	/**
	 * Find the slope of a line between two Point2Ds
	 * 
	 * @param pt1
	 *            first Point2D
	 * @param pt2
	 *            second Point2D
	 * @return the value of the slope
	 */
	public static double slope(Point2D pt1, Point2D pt2) {
		return (pt1.getY() - pt2.getY()) / (double) (pt1.getX() - pt2.getX());
	}

	/**
	 * Intersect a line with Ox axis
	 * 
	 * @param pt
	 *            the Point2D which is part of a line
	 * @param toIntersect
	 *            value of the X Point2D from Ox
	 * @param slope
	 *            slope of the line
	 * @return Point2D on Oy axis on which the two Point2Ds intersect
	 */
	public static double intersectWithOx(Point2D pt, Point2D toIntersect,
			double slope) {
		return slope * (toIntersect.getX() - pt.getX()) + pt.getY();
	}

	/**
	 * Verify if a value is in a certain range
	 * @param left limit left
	 * @param right limit right
	 * @param value value to be tested
	 * @return true if the value is in range or false otherwise
	 */
	public static boolean isInRange(double left, double right, double value) {
		if (left <= value && value <= right)
			return true;

		return false;
	}

	/**
	 * Get the value of the power associated with the cost
	 * 
	 * @param cost
	 *            the cost of the antenna
	 * @return the power associated with the cost
	 */
	public static int getPower(int cost) {
		Set<Integer> powerSet = powerCost.keySet();
		Iterator<Integer> powerSetIter = powerSet.iterator();
		Integer powerValue = -1;

		while (powerSetIter.hasNext()) {
			powerValue = powerSetIter.next();

			if (powerCost.get(powerValue) == cost)
				break;
		}

		return powerValue;
	}

	private static void setInitialPoint(Point2D ptOx, Point2D ptOy,
			int direction) {
		switch (direction) {
		case SV_DIR:
			ptOx.setLocation(ptOx.getX() + 1, 0);
			break;
		case SE_DIR:

			break;
		case NE_DIR:
			ptOy.setLocation(0, ptOy.getY() - 1);
			break;
		case NV_DIR:
			ptOx.setLocation(ptOx.getX() + 1, 0);
			ptOy.setLocation(0, ptOy.getY() - 1);
			break;
		}
	}

	/**
	 * Count the number of points that will be shadowed using this direction
	 * 
	 * @param source
	 *            source point of the line
	 * @param destination
	 *            destination point of the line
	 * @param direction
	 *            direction in which the line will be drawn
	 * @return number of shadowed points
	 */
	public static int noPoint2DShadowed(Point2D source, Point2D destination,
			int direction) {
		int noShadowedPts = 0, noVisited = 0;
		int line = (int) destination.getX();
		int column = (int) destination.getY();
		int initLine = (int) source.getX();
		int initColumn = (int) source.getY();

		boolean leaveLoop = false;

		Point2D srcCopy = new Point2D.Double(source.getY() + 0.5,
				-source.getX() - 0.5);
		Point2D destCopy = new Point2D.Double(destination.getY() + 0.5,
				-destination.getX() - 0.5);

		Point2D ptOx = new Point2D.Double(column, 0);
		Point2D ptOy = new Point2D.Double(0, -line);

		double slope = slope(srcCopy, destCopy);
		double oxIntersect, oyIntersect;

		// initialize the intersection line
		setInitialPoint(ptOx, ptOy, direction);

		while (!leaveLoop) {
			// intersect line with the two axes
			oxIntersect = intersectWithOx(destCopy, ptOx, slope);
			oyIntersect = intersectWithOy(destCopy, ptOy, slope);
			noVisited++;

			switch (direction) {
			case SE_DIR:
				// the side that intersect the line and advance in that
				// direction
				if (source.getX() == source.getY()
						&& destination.getX() == destination.getY()) {
					line--;
					column--;
				} else if (isInRange(ptOx.getX(), ptOx.getX() + 1, oyIntersect)) {
					line--;
				} else if (isInRange(ptOy.getY() - 1, ptOy.getY(), oxIntersect)) {
					column--;
				} else {
					leaveLoop = true;
					noShadowedPts = 0;
				}
				break;
			case SV_DIR:
				// the side that intersects the line and advance in that
				// direction
				if (slope == 1) {
					line--;
					column++;
				} else if (isInRange(ptOx.getX() - 1, ptOx.getX(), oyIntersect)) {
					line--;
				} else if (isInRange(ptOy.getY() - 1, ptOy.getY(), oxIntersect)) {
					column++;
				} else {
					leaveLoop = true;
					noShadowedPts = 0;
				}
				break;
			case NE_DIR:
				// the side that intersect the line and advance in that
				// direction
				if (slope == 1) {
					line++;
					column--;
				} else if (isInRange(ptOx.getX(), ptOx.getX() + 1, oyIntersect)) {
					line++;
				} else if (isInRange(ptOy.getY(), ptOy.getY() + 1, oxIntersect)) {
					column--;
				} else {
					leaveLoop = true;
					noShadowedPts = 0;
				}
				break;
			case NV_DIR:
				// the side that intersect the line and advance in that
				// direction
				if (source.getX() == source.getY()
						&& destination.getX() == destination.getY()) {
					line++;
					column++;
				} else if (isInRange(ptOx.getX() - 1, ptOx.getX(), oyIntersect)) {
					destCopy.setLocation(ptOx);
					line++;
				} else if (isInRange(ptOy.getY(), ptOy.getY() + 1, oxIntersect)) {
					destCopy.setLocation(ptOy);
					column++;
				} else {
					leaveLoop = true;
					noShadowedPts = 0;
				}
				break;
			}
			ptOx.setLocation(column, 0);
			ptOy.setLocation(0, -line);

			if (line == initLine && column == initColumn) {
				leaveLoop = true;
			}

			// verify the height of the point
			if (Utils.initialMatrix[line][column] > Utils.initialMatrix[initLine][initColumn])
				noShadowedPts += noVisited;
		}
		return noShadowedPts;
	}

	/**
	 * Intersect a line with Oy axis
	 * 
	 * @param pt
	 *            the Point2D which is part of a line
	 * @param toIntersect
	 *            value of the y Point2D from Oy
	 * @param slope
	 *            slope of the line
	 * @return Point2D on Ox axis on which the two Point2Ds intersect
	 */
	public static double intersectWithOy(Point2D pt, Point2D toIntersect,
			double slope) {
		return ((toIntersect.getY() - pt.getY()) / slope) + pt.getX();
	}

	public static void printInitialMatrix() {
		System.out.println("No lines " + N_ROWS + " No cols " + N_COLS);
		for (int i = 0; i < N_ROWS; ++i) {
			for (int j = 0; j < N_COLS; ++j) {
				System.out.print(initialMatrix[i][j] + "\t");
			}
			System.out.println();
		}
	}
}
