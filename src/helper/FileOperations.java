package helper;

import genetic_info.Chromosome;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

public class FileOperations {
	
	
	private static void saveInfoFirstLine(String firstLine) {
		String arrayLine[];
		
		arrayLine = firstLine.split("\\s+");
		Utils.N_ROWS = Integer.valueOf(arrayLine[0]);
		Utils.N_COLS = Integer.valueOf(arrayLine[1]);
		Utils.MAX_HEIGHT = Integer.valueOf(arrayLine[2]);
		Utils.N_ANTENNAS = Integer.valueOf(arrayLine[3]);
	}
	
	private static void saveInfoSecondLine(String secondLine) {
		String arrayLine[];
		
		arrayLine = secondLine.split("\\s+");
		Utils.BUDGET = Integer.valueOf(arrayLine[0]);
		Utils.P_MIN = Integer.valueOf(arrayLine[2]);
		Utils.P_MAX = Integer.valueOf(arrayLine[3]);
	}
	
	private static void saveInfoPowerAndCost(BufferedReader buffReader) throws IOException {
		int noLines = Utils.P_MAX - Utils.P_MIN + 1;
		String line;
		String infoArray[];
		
		Utils.powerCost = new HashMap<Integer, Integer>();

		for (int i = 0; i < noLines; ++i) {
				line = buffReader.readLine();
				infoArray = line.split("\\s+");
				Utils.powerCost.put(Integer.valueOf(infoArray[0]), Integer.valueOf(infoArray[1]));
		}
	}
	
	private static void saveInputMatrix(BufferedReader buffReader) throws IOException {
		String line;
		String arrayInfo[];
		int noLines = Utils.N_ROWS;
		
		Utils.initialMatrix = new int[Utils.N_ROWS][Utils.N_COLS];
		
		for (int i = 0; i < noLines; ++i) {
			line = buffReader.readLine();
			arrayInfo = line.split("\\s+");
			
			for (int j = 0; j < Utils.N_COLS; ++j) {
				Utils.initialMatrix[i][j] = Integer.valueOf(arrayInfo[j]);
			}
			
		}
	}
	
	public static void parseInputFile() {
		String line;
		FileInputStream inFile;
		
		Scanner sc = new Scanner(System.in);
		
		//get input fileName
		System.out.print("Input file name: ");
		Utils.inputFilename = sc.next();
		
		System.out.print("Output file name: ");
		Utils.outputFilename = sc.next();
		
		sc.close();
		
		//open input file
		try {
			inFile = new FileInputStream(Utils.inputFilename);
			DataInputStream inStream = new DataInputStream(inFile);
			BufferedReader buffReader = new BufferedReader(new InputStreamReader(inStream));
			
			// save the first line
			line = buffReader.readLine();
			saveInfoFirstLine(line);
			
			//save the second line
			line = buffReader.readLine();
			saveInfoSecondLine(line);
			
			//save connection between power value and the cost to put an antenna
			saveInfoPowerAndCost(buffReader);
			
			//read input matrix
			saveInputMatrix(buffReader);
			
			buffReader.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Write info about the found antennas
	 * @param solution chromosomes that are the solution
	 */
	public static void writeOutputFile(List<Chromosome> solution) {
		Chromosome tmpChr;
		int power;
		try {
			FileWriter fileWriter = new FileWriter(Utils.outputFilename);
			BufferedWriter buffWriter = new BufferedWriter(fileWriter);
			
			buffWriter.write("0" + " 0\n");
			
			for (int i = 0; i < solution.size(); ++i) {
				tmpChr = solution.get(i);
				power = Utils.getPower(tmpChr.getFitness());
				
				buffWriter.write(tmpChr.getPosition().getX() + 
						" " + tmpChr.getPosition().getY() +
						" " + tmpChr.getDirection() +
						" " + power + "\n");
			}
			
			buffWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	
	
}
