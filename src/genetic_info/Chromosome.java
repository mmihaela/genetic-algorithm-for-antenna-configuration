package genetic_info;

import java.awt.geom.Point2D;
import java.util.ArrayList;

import helper.Utils;

public class Chromosome implements Comparable<Chromosome>{
	
	private int fitness;
	private int direction;
	private double selectionProbability;
	private int[] value;
	private Point2D position;
	
	public static ArrayList<Chromosome> crossover(Chromosome first, Chromosome second) {
		Chromosome newFirst = new Chromosome();
		Chromosome newSecond = new Chromosome();
		
		newFirst.value = new int[first.value.length];
		newSecond.value = new int[second.value.length];
		
		
		ArrayList<Chromosome> crossOver = new ArrayList<Chromosome>();
		
		for (int i = 0; i < first.value.length; ++i) {
			if ( i < Utils.CROSSOVER_POINT) {
				newFirst.value[i] = first.value[i];
				newSecond.value[i] = second.value[i];
			} else {
				newFirst.value[i] = second.value[i];
				newSecond.value[i] = first.value[i];
			}
		}
		
		crossOver.add(newFirst);
		crossOver.add(newSecond);
		
		return crossOver;
	}

	@Override
	public int compareTo(Chromosome o) {
		if (this == o)
			return 0;
		
		if (this.fitness < o.fitness)
			return 1;
		
		if (this.fitness == o.fitness)
			return 0;
		
		return -1;
	}


	private static void copyIntoChromosome(int[] currentChr,
										int startLine, int stopLine,
										int startColumn, int stopColumn) {
		
		for (int i = startLine; i < stopLine; ++i)
			for (int j = startColumn; j < stopColumn; ++j) {
				currentChr[i * Utils.N_COLS + j] = Utils.ACTIVE;
			}
	}
	
	/**
	 * Initialize the chromosomes and put 1 only on one position of
	 * the chromosome
	 * @return all the chromosomes associated with the given board
	 */
	public static ArrayList<Chromosome>  initialChromosomes() {
		int startLine, stopLine, startColumn, stopColumn;
		int tmp[];
		int positiveCells = Utils.P_MIN;//(Utils.P_MAX - Utils.P_MIN + 1)/2;
		Chromosome tmpChr;
		ArrayList<Chromosome> chromosomeList = new ArrayList<Chromosome>();
		
		for (int i = 0; i < Utils.N_ROWS; ++i)
			for (int j = 0; j < Utils.N_COLS; ++j) {
				tmpChr = new Chromosome();
				tmp = new int[Utils.N_ROWS * Utils.N_COLS];
				Utils.setToZero(tmp);
				
				startLine = (i - positiveCells < 0)? 0 : i - positiveCells;
				stopLine = (i + positiveCells > Utils.N_ROWS)? Utils.N_ROWS: i + positiveCells;
				
				startColumn = (j - positiveCells < 0)? 0 : j - positiveCells;
				stopColumn = (j + positiveCells > Utils.N_COLS)? Utils.N_COLS: j + positiveCells;
				
				copyIntoChromosome(tmp, startLine, stopLine, startColumn, stopColumn);
				tmpChr.setValue(tmp);
				chromosomeList.add(tmpChr);
			}
		
		return chromosomeList;
	}
	
	/**
	 * Compute probability to select a chromosome
	 * @param fitness the fitness of the chromosome
	 * @param fitnessSum the sum of all fitnesses of the chromosomes
	 * @return the probability associates with the chromosome
	 */
	public void probability(int fitnessSum) {
		setSelectionProbability(fitness/(double)fitnessSum);
	}
	
	public int getFitness() {
		return fitness;
	}

	public void setFitness(int fitness) {
		this.fitness = fitness;
	}

	public int[] getValue() {
		return value;
	}

	public void setValue(int[] value) {
		this.value = value;
	}

	public double getSelectionProbability() {
		return selectionProbability;
	}

	public void setSelectionProbability(double selectionProbability) {
		this.selectionProbability = selectionProbability;
	}

	public int getDirection() {
		return direction;
	}

	public void setDirection(int direction) {
		this.direction = direction;
	}

	public Point2D getPosition() {
		return position;
	}

	public void setPosition(Point2D position) {
		this.position = position;
	}
	
	public String toString() {
		return position.toString() + " " + fitness + " " + direction;
	}
	
}