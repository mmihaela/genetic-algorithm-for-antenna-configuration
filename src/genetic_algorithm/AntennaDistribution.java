package genetic_algorithm;

import helper.FitnessUtils;
import helper.Utils;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import genetic_info.Chromosome;

public class AntennaDistribution {
	static ArrayList<Chromosome> chromosomeList;
	static ArrayList<Chromosome> solution;

	/**
	 * Set the ranges for roulette selection
	 * 
	 * @param totalFitness
	 *            sum of fitness values of all chromosomes
	 */
	public static void rouletteSelection(int totalFitness) {
		Chromosome tmp;
		double valuesUntilNow = 0;
		double currentProbability;

		for (int i = 0; i < chromosomeList.size(); ++i) {
			tmp = chromosomeList.get(i);
			
			currentProbability = tmp.getFitness() / (double) totalFitness;
			valuesUntilNow += currentProbability;

			// set probability for the chromosome to be chosen
			tmp.setSelectionProbability(valuesUntilNow);
		}
	}

	/**
	 * Obtains the parent chromosomes for a random generated value in range
	 * [0,1]
	 * 
	 * @return the parents
	 */
	public static ArrayList<Chromosome> getParents() {
		Chromosome tmp;
		Random random = new Random();
		double randValue = random.nextDouble();
		ArrayList<Chromosome> parents = new ArrayList<Chromosome>(2);
		
		for (int i = 0; i < chromosomeList.size(); ++i) {
			tmp = chromosomeList.get(i);

			if (randValue < tmp.getSelectionProbability()) {
				parents.add(tmp);
				if (i == 0) {
					parents.add(chromosomeList.get(chromosomeList.size() - 1));
				} else {
					parents.add(chromosomeList.get(i - 1));
				}
				break;
			}
		}

		return parents;

	}

	/**
	 * Apply fitness function on a given chromosome: choose a point from the
	 * chromosome which will be the antenna and relativ to it compute the
	 * distance The fitness is the cost associated with the power of the antenna
	 * 
	 * @param chromosome
	 * @return
	 */
	public static void fitness(Chromosome chromosome) {
		int fitnessValue = 0;
		int fitnessValueAux, direction;
		Point2D pointSE, pointSV, pointNE, pointNV, solutionPoint;

		// the most SE point
		pointSE = FitnessUtils.mostSEpoint(chromosome);

		// the most SV point
		pointSV = FitnessUtils.mostSVpoint(chromosome);

		// the most NE point
		pointNE = FitnessUtils.mostNEpoint(chromosome);

		// the most NV point
		pointNV = FitnessUtils.mostNVpoint(chromosome);

		// compute fitness value for each direction
		fitnessValue = FitnessUtils.fitnessDependingDirection(chromosome,
				pointSE, Utils.SE_DIR);
		direction = Utils.SE_DIR;
		solutionPoint = pointSE;

		fitnessValueAux = FitnessUtils.fitnessDependingDirection(chromosome,
				pointSV, Utils.SV_DIR);
		if (fitnessValueAux > fitnessValue) {
			fitnessValue = fitnessValueAux;
			direction = Utils.SV_DIR;
			solutionPoint = pointSV;
		}

		fitnessValueAux = FitnessUtils.fitnessDependingDirection(chromosome,
				pointNE, Utils.NE_DIR);
		if (fitnessValueAux > fitnessValue) {
			fitnessValue = fitnessValueAux;
			direction = Utils.NE_DIR;
			solutionPoint = pointNE;
		}

		fitnessValueAux = FitnessUtils.fitnessDependingDirection(chromosome,
				pointNV, Utils.NV_DIR);
		if (fitnessValueAux > fitnessValue) {
			fitnessValue = fitnessValueAux;
			direction = Utils.NV_DIR;
			solutionPoint = pointNV;
		}

		// update the characteristics of the chromosome
		chromosome.setFitness(fitnessValue);
		chromosome.setDirection(direction);
		chromosome.setPosition(solutionPoint);

	}

	/**
	 * Evolution from a generation to an other
	 */
	public static ArrayList<Chromosome> evolve() {
		ArrayList<Chromosome> nextGeneration = new ArrayList<Chromosome>();
		ArrayList<Chromosome> parents;

		Chromosome tmpChromosome;

		int totalFitness = 0;

		// compute the fitness for each chromosome
		for (int i = 0; i < chromosomeList.size(); ++i) {
			tmpChromosome = chromosomeList.get(i);
			fitness(tmpChromosome);
			totalFitness += tmpChromosome.getFitness();
		}

		// compute probability for each chromosome
		rouletteSelection(totalFitness);

		// order chromosomes in decreasing order after fitness
		Collections.sort(chromosomeList);

		// use elitism to preserve the best chromosomes
		for (int i = 0; i < Utils.ELITISM; ++i) {
			nextGeneration.add(chromosomeList.get(i));
		}

		// using roulette crossover points
		for (int i = 0; i < ((Utils.N_ROWS * Utils.N_COLS) / 2) - Utils.ELITISM; ++i) {
			parents = getParents();
			parents = Chromosome.crossover(parents.get(0), parents.get(1));
			nextGeneration.addAll(parents);
		}

		solution = new ArrayList<Chromosome>();
		solution.addAll(chromosomeList);
		return nextGeneration;
	}

	public static List<Chromosome> findAntennas() {
		ArrayList<Chromosome> newGeneration;
		List<Chromosome> finalSolution = new ArrayList<Chromosome>();
		int currentCost = 0;
		int solutionCost = -1;
		int counter = 0;

		for (counter = 0; counter < Utils.MAX_NO_ITER; ++counter, currentCost = 0) {
			// get the next generation
			newGeneration = evolve();

			// verify the possible solutions
			for (int i = 0; i < Utils.N_ANTENNAS; ++i) {
				currentCost += solution.get(i).getFitness();
			}

			// verify if a better solution was found
			if (currentCost > solutionCost && currentCost <= Utils.BUDGET) {
				finalSolution.clear();
				finalSolution = solution.subList(0, Utils.N_ANTENNAS);
				solutionCost = currentCost;
			}

			// the chromosome list is updated
			chromosomeList.clear();
			chromosomeList.addAll(newGeneration);
		}
		
		return finalSolution;
	}

}
