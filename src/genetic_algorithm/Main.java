package genetic_algorithm;

import java.util.List;

import genetic_info.Chromosome;
import helper.FileOperations;
import helper.Utils;

public class Main {
	
	public static void main(String args[]) {
		List<Chromosome> solution;
		
		//save info from input file
		FileOperations.parseInputFile();
		
		//create the initial chromosomes
		AntennaDistribution.chromosomeList = Chromosome.initialChromosomes();
		
		//update chromosome cross point
		Utils.CROSSOVER_POINT = 2 * (Utils.N_COLS * Utils.N_ROWS)/3;
		
		//try to find the solution
		solution = AntennaDistribution.findAntennas();
		
		FileOperations.writeOutputFile(solution);
	}

}
